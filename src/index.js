import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';

class FilterableProductTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      inStock: true
    }

    this.handleSearchText = this.handleSearchText.bind(this);
    this.handleInStock = this.handleInStock.bind(this);
  }

  handleSearchText(event) {
    this.setState({
      searchText: event.target.value,
    });
  }

  handleInStock() {
    this.setState({
      inStock: !this.state.inStock,
    });
  }

  render() {
    return (
      <div className="root-box">
        <SearchBar
          searchText={this.state.searchText}
          inStock={this.state.inStock}
          handleInStock={this.handleInStock}
          handleSearchText={this.handleSearchText}
        />
        <br />
        <ProductTable
          searchText={this.state.searchText}
          inStock={this.state.inStock}
          data={this.props.data}
        />
      </div>
    );
  }

}

class SearchBar extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="search-box">
        <input type="text" value={this.props.searchText} placeholder="Search..." onChange={this.props.handleSearchText} />
        <br />
        <input type="checkbox" checked={this.props.inStock} onChange={this.props.handleInStock} /> Only show product in stock
      </div>
    );
  }

}

class ProductTable extends React.Component {

  constructor(props) {
    super(props);
  }

  renderProductTableHeader() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
        </tr>
      </thead>
    );
  }

  renderProductTableBodyTitle(object) {
    return (
      <tr key={object.category}>
        <td colSpan='2'>{object.category}</td>
      </tr>
    );
  }

  renderProductTableBodyList(object, index) {
    return (
      <tr key={index} className={object.stocked ? '' : 'not-in-stock'}>
        <td>{object.name}</td>
        <td>{object.price}</td>
      </tr>
    );
  }

  renderProductTableBody() {

    let category = '';
    let showCategory = false;

    let searchText = this.props.searchText;
    let inStock = this.props.inStock;

    return (
      <tbody>
      {
        this.props.data
        .filter(function(object) {
          if (inStock)
            return ( object.stocked ? object : null );
          return object;
        })
        .filter(function(object) {
          if (searchText.length >= 3) {
            if (object.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0 ||
              object.category.toLowerCase().indexOf(searchText.toLowerCase()) >= 0)
              return object;
          } else
            return object;
         })
        .map((object, index) => {
          showCategory = false;
          if (category != object.category) {
            category = object.category;
            showCategory = true;
          }
          return ([
            ( showCategory && this.renderProductTableBodyTitle(object) ),
            ( this.renderProductTableBodyList(object, index) )
          ]);
        })
      }
      </tbody>
    );
  }

  render() {
    return (
      <div>
        <table>
          {this.renderProductTableHeader()}
          {this.renderProductTableBody()}
        </table>
      </div>
    );
  }

}

let json = [
  {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
  {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
  {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
  {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
  {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
  {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
];

ReactDOM.render(
  <FilterableProductTable data={json}/>,
  document.getElementById('root')
);